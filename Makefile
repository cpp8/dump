CXX=clang
EXEC := dump
SOURCES = $(wildcard *.cpp)
HEADERS = $(wildcard *.hpp) 
OBJECTS = $(SOURCES:.cpp=.o)

CXXFLAGS := -std=gnu++17
LDFLAGS := -lstdc++ -lstdc++fs
all: $(OBJECTS) $(HEADERS)
	$(CXX) $(OBJECTS) $(CXXFLAGS) $(LDFLAGS) -o $(EXEC)

clean:
	ls $(SOURCES)
	ls $(OBJECTS)
	rm -f $(EXEC) $(OBJECTS)

%.o:%.cpp
	$(CXX) $(CXXFLAGS) -c $(<)