#include <iostream>
#include <fstream>
#include <iomanip>
#include <filesystem>
#include <string>
#include "options.hpp"
#include "dumpfile.hpp"

string dashes = "================================================================================================" ;

void dumpBlock(char *block, int blocklen, int offset) {
    cout << setw(10) << setfill('0') << dec << offset << " : " ;

    for (int i=0; i < blockSize; i++ ) {
        if (i < blocklen) {
            if (isprint(block[i])) {
                cout << block[i] ;
            } else {
                cout << "." ;
            }
        } else {
            cout << " " ;
        }
    }
    cout << " * " ;

    for (int i=0; i < blockSize ; i++ ) {
        if (i < blocklen) {
            switch (outputBase) {
                case HEXADECIMAL:
                    cout << setw(2) << setfill('0') << hex << int(block[i]) ;
                    break;
                case OCTAL:
                    cout << setw(3) << setfill('0') << oct << int(block[i]) ;
                    break;            
                default:
                    break;
            }
            cout << " " ;
        } else {
            switch (outputBase) {
                case HEXADECIMAL:
                    cout << "   " ;
                    break;
                case OCTAL:
                    cout << "    " ;
                    break;
                break;
            }
        }
    }
    cout << " * " ;

    cout << endl ;
}

void dumpFile(string filename) {
    ifstream ifile(filename, ios::binary) ;
    if (!ifile.is_open()) {
        cout << "Error opening " << filename << endl ;
        return ;
    }

    cout << endl << dashes << endl ;
    cout << std::filesystem::absolute(filename) << endl ;
    cout << dashes << endl ;
    cout << endl ;

    int offset =0 ;
    char buffer[blockSize] ;

    while (!ifile.eof()) {
        ifile.read(buffer,blockSize) ;
        if (ifile.gcount() > 0) {
            dumpBlock(buffer, ifile.gcount() , offset);
            offset += ifile.gcount() ;
        }
    }
    ifile.close();
}