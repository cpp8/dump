#ifndef _OPTIONS_HPP_
#define _OPTIONS_HPP_

#include <string>
using namespace std;

const int BLOCKSIZE_DEFAULT = 16 ;

typedef enum {
  UNDEFINED_BASE ,
  HEXADECIMAL ,
  OCTAL
} OUTPUT_BASE ;

const OUTPUT_BASE BASE_DEFAULT = HEXADECIMAL ;

extern bool verbose ;
extern int blockSize ;
extern OUTPUT_BASE outputBase ;

extern void showHelp() ;
extern int getNextArgument(int argc, char **argv) ;

#endif
