#include <iostream>
#include <unistd.h>
using namespace std ;
#include "options.hpp"

bool verbose = false ;
int blockSize = BLOCKSIZE_DEFAULT ;
OUTPUT_BASE outputBase = BASE_DEFAULT ;

void showHelp() {
    cerr << "dump - produce a hex or octal dump of a file. Version 1 " << __TIMESTAMP__ << endl ;
    cerr << "usage: dump [options] file1 file2..." << endl ;
    cerr << "\t-h\t Help" << endl ;
    cerr << "\t-v\t Verbose" << endl ;
    cerr << "\t-b n\t Blocksize n" << endl ;
    cerr << "\t-o\t Octal dump" << endl ;
    cerr << "\t-x\t Hexadecimal dump" << endl ;
}

int getNextArgument(int argc, char **argv) {

    int opt ;

    if (argc < 2) {
        showHelp();
        exit(EXIT_FAILURE);
    }

    if (optind >= argc) {
        return optind ;
    }

    while ((opt = getopt(argc, argv, "b:hovx")) != -1) {
        switch (opt) {
        case 'h':
            showHelp() ;
            return argc ;
        case 'v':
            verbose = true ;
            cerr << "Verbose" << endl ;
            continue ;
        case 'b':
            blockSize = atoi(optarg) ;
            if (verbose) {
                cerr << "Blocksize set to " << blockSize << endl ;
            }
            continue ;
        case 'o':
            outputBase = OCTAL ;
            if (verbose) {
                cerr << "Dump in octal" << endl ;
            }
            continue;
        case 'x':
            outputBase = HEXADECIMAL ;
            if (verbose) {
                cerr << "Dump in hexadecimal" ;
            }
            continue;
        default: 
            showHelp();
            exit(EXIT_FAILURE);
        }
    }
    return optind ;
}
