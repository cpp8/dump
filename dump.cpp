#include <iostream>
#include "options.hpp"
#include "dumpfile.hpp"
using namespace std ;
int main(int argc, char **argv) {
    if (argc < 2) {
        showHelp() ;
        return 0 ;
    }
    int nextfile = getNextArgument(argc, argv) ;
    for (; nextfile < argc; nextfile++) {
        dumpFile(argv[nextfile]) ;
    }
    return 0 ;
}